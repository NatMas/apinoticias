import { Injectable } from '@angular/core';
import {Storage} from '@ionic/storage-angular'
import { Article } from '../interfaces/interfaces';
@Injectable({
  providedIn: 'root'
})
export class DataLocalService {
  noticias : Article[] = []
  constructor(private storage: Storage) {
    this.storage.create();
   }

   guardarNoticia(noticia:Article){
     this.noticias.unshift(noticia);
     this.storage.set('favoritos',this.noticias);
   }

   async cargarFavoritos(){
    const fav = await this.storage.get('favoritos');
    if(fav){
      this.noticias  = fav;
    }
    
  }
}
