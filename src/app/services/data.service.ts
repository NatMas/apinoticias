import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { RespuestaNoticias } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http:HttpClient) { }

  getNoticies(){
    return this.http.get<RespuestaNoticias>(`https://newsapi.org/v2/top-headlines?language=es&apiKey=${environment.apikey}`)
    
  }
  getCategorias(category:string){
    return this.http.get<RespuestaNoticias>(`https://newsapi.org/v2/top-headlines?country=us&category=${category}&apiKey=${environment.apikey}`)
    
  }
}