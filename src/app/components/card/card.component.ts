import { Component } from '@angular/core';
import { Article } from '../../interfaces/interfaces';
import { DataService } from '../../services/data.service';
import { ActionSheetController } from '@ionic/angular';

import { Plugins } from '@capacitor/core';
import { DataLocalService } from 'src/app/services/data-local.service';

const { Share } = Plugins;
@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent  {

  news:Array<Article> = [];
  slice : number =1;
  constructor(private data : DataService,public actionSheetController: ActionSheetController,private fav : DataLocalService) {
   this.loadNew()
  }
  loadNew(event?){
  this.slice = this.slice+10
    this.data.getNoticies().subscribe(
      resp =>{
        //console.log('News',resp);
        this.news.push(...resp.articles)
        if(event){
          
          event.target.complete()
        }
      }
    )
    //console.log(this.news)
  }

  async abrirnoticia(webb){
    const { Browser } = Plugins;
    await Browser.open({ url: webb });
    //console.log(webb)
   }

   async lanzarmenu(noticia: Article){
     console.log(noticia)
    const actionSheet = await this.actionSheetController.create({
      header: 'Menu',
      cssClass: 'my-custom-class',
      buttons: [ {
        text: 'Share',
        icon: 'share',
         handler: () => {
          let shareRet = {
            title: noticia.title,
            text: noticia.content,
            url: noticia.url,
            dialogTitle: 'Noticia'
          };
          console.log(shareRet);
          /*
          let shareRet = Share.share({
            title: noticia.title,
            text: noticia.content,
            url: noticia.url,
            dialogTitle: 'Noticia'
          });
          
          */
        }
      },{
        text: 'Favorite',
        icon: 'heart',
        handler: () => {
          
          console.log('Favorite clicked');
          this.fav.guardarNoticia(noticia)
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
   }
  }

