import { Component } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Article } from '../../interfaces/interfaces';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  news:Array<Article> = []
  constructor(private data : DataService) {
   this.loadNew()
  }
    loadNew(event?){
      this.data.getNoticies().subscribe(
        resp =>{
          //console.log('News',resp);
          this.news.push(...resp.articles)
          if(event){
            event.target.complete()
          }
        }
      )
      console.log(this.news)
    }
}
