import { Component } from '@angular/core';
import { Article } from '../../interfaces/interfaces';
import { DataService } from '../../services/data.service';

import { Plugins } from '@capacitor/core';
import { DataLocalService } from 'src/app/services/data-local.service';
import { ActionSheetController } from '@ionic/angular';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  news:Array<Article> = []
  categorias:Array<string> = ["business","entertainment","general"]
  constructor(private data : DataService,private fav : DataLocalService,public actionSheetController: ActionSheetController) {
    this.loadNew(this.categorias[0])
   }
     loadNew(categoria,event?){
       this.news = []
       this.data.getCategorias(categoria).subscribe(
         resp =>{
           console.log('News',resp);
           this.news.push(...resp.articles)
           if(event){
            event.target.complete()
          }
         }
       )
       console.log(this.news)
     }

     async abrirnoticia(webb){
      const { Browser } = Plugins;
      await Browser.open({ url: webb });
      console.log(webb)
     }
     async lanzarmenu(noticia: Article){
      console.log(noticia)
     const actionSheet = await this.actionSheetController.create({
       header: 'Menu',
       cssClass: 'my-custom-class',
       buttons: [ {
         text: 'Share',
         icon: 'share',
         handler: () => {
          let shareRet = {
            title: noticia.title,
            text: noticia.content,
            url: noticia.url,
            dialogTitle: 'Noticia'
          };
          console.log(shareRet);
         }
       },{
         text: 'Favorite',
         icon: 'heart',
         handler: () => {
           
           console.log('Favorite clicked');
           this.fav.guardarNoticia(noticia)
         }
       }, {
         text: 'Cancel',
         icon: 'close',
         role: 'cancel',
         handler: () => {
           console.log('Cancel clicked');
         }
       }]
     });
     await actionSheet.present();
    }
}
