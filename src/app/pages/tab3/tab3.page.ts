import { Component } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Article } from 'src/app/interfaces/interfaces';
import { DataLocalService } from 'src/app/services/data-local.service';
import { Plugins } from '@capacitor/core';


@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  news:Array<Article> = []

  constructor(public dataLocalService: DataLocalService,public actionSheetController: ActionSheetController) {
    this.dataLocalService.cargarFavoritos();
    this.news = this.dataLocalService.noticias;    
  }
  async abrirnoticia(webb){
    const { Browser } = Plugins;
    await Browser.open({ url: webb });
    //console.log(webb)
   }

   ionViewDidEnter(){
    this.dataLocalService.cargarFavoritos();
    this.news = this.dataLocalService.noticias;   
   }

  async lanzarmenu(noticia: Article){
    console.log(noticia.title)

  const actionSheet = await this.actionSheetController.create({
    header: 'Menu',
    cssClass: 'my-custom-class',
    buttons: [ {
      text: 'Share',
      icon: 'share',
      handler: () => {
        let shareRet = {
          title: noticia.title,
          text: noticia.content,
          url: noticia.url,
          dialogTitle: 'Noticia'
        };
        console.log(shareRet);
      }
    },{
      text: 'Quitar Fav',
      icon: 'heart',
      handler: () => {
        
        console.log('Favorite clicked');
        this.news = this.news.filter(noti => noti.title !==noticia.title)
      }
    }, {
      text: 'Cancel',
      icon: 'close',
      role: 'cancel',
      handler: () => {
        console.log('Cancel clicked');
      }
    }]
  });
  await actionSheet.present();
 }
  

}
